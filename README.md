# Bitcoin Dashboard for Dun & Bradstreet in Vue.js

#### Candidate: Sarah Rudder

---
## Exercise

Create a small application using Vue.js that hits an API (in this case Bitcoin prices in realtime - see **Technical Specs** below) and display corresponding results.

### Requirements

* Display each currency denomination's **Last, Buy and Sell price** from blockchain.info service (see **Technical Specs** below)
* Add a **button to query** for fresh data.
* Use at least **one element from D&B's product design guide** : https://productdesign.dnb.com/developers/
* Use some of the **existing D&B product stylesheet** to make your application feel right at home in the D&B family (see **Technical Specs** below).
* Use at least one **CSS transition**.
* Show off your **responsive** know-how by hiding/displaying something only at a specific view size defined by you.

### Technical Specs

D&B Product Design Guide: https://productdesign.dnb.com/developers/

Service to hit: https://blockchain.info/ticker

D&B Stylesheet to use: https://productdesign.dnb.com/wp-content/themes/dnb-product-style-guide/library/dnb-product-design-guide/css/dnb-product-design-guide.css

### Delivery
Please provide links to a working, live example and the source code.
