// rewriting provided template from sample: https://github.com/pstephan1187/vue-datatable/blob/master/src/vue-datatable-header.vue

Vue.component('datatable-header', {
  template: '\
  	<th scope="col" v-if="column.sortable"\
    @click="toggleSort" >\
  		{{ column.label }}\
  		<span></span>\
  	</th>\
  ',
  props: {
		model: {
			prop: 'direction',
			event: 'change'
		},
		column: [Object, Array],
		settings: Object,
		direction: {
			type: String,
			default: null
		}
	},
	computed: {
		canSort(){
			return this.column.sortable;
		},
		is_sorted_ascending(){
			return this.direction === 'asc';
		},
		is_sorted_descending(){
			return this.direction === 'desc';
		},
		is_sorted(){
			return this.is_sorted_descending || this.is_sorted_ascending;
		},
		classes(){
			var available_classes = this.settings.get('table.sorting.classes');
			var classes = available_classes.canSort;

			if(!this.canSort){
				return '';
			}

			if(!this.is_sorted){
				classes = classes.concat(available_classes.sortNone);

				return this.joinClasses(classes);
			}

			if(this.is_sorted_ascending){
				classes = classes.concat(available_classes.sortAsc);
			}

			if(this.is_sorted_descending){
				classes = classes.concat(available_classes.sortDesc);
			}

			return this.joinClasses(classes);
		}
	},
	methods: {
		joinClasses(classes){
			return this.unique(classes).join(' ');
		},
		toggleSort(){
			if(!this.direction || this.direction === null){
				this.$emit('change', 'asc', this.column);
			}else if(this.direction === 'asc'){
				this.$emit('change', 'desc', this.column);
			}else{
				this.$emit('change', null, this.column);
			}

			return;
		},
		unique(ar) {
		    var seen = {};

		    return ar.filter(function(item) {
		        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
		    });
		}
	}
})

var app = new Vue({
  el: '#app',
  data: {
    appTitle: "Bitcoin Dashboard",
    refreshButtonText: "Refresh",
    currencyTableTitle: "Recent Bitcoin Prices",
    currencyDataJson: null,
    currencyTableHeaders: {
      "denomination" : "Currency",
      "sell" : "Current Sell Price",
      "buy" : "Current Buy Price",
      "last" : "Last Price"
    },
    lastFetched : "",
    lastFetchedLabel: "Last Fetched ",
    lastFetchedTime: null,
    nowFetching: false,
    columns: [
      {label: 'Currency', representedAs: function(row){
        return row.currencyName + ' (' + row.symbol + ')';}
      },
      {label: 'Current Sell Price', representedAs: function(row){
        return row.symbol + app.formatCurrencyAsString(row.sell);}
      },
      {label: 'Current Buy Price', representedAs: function(row){
        return row.symbol + app.formatCurrencyAsString(row.buy);}
      },
      {label: 'Last Price', representedAs: function(row){
        return row.symbol + app.formatCurrencyAsString(row.last);}
      },
    ],
    rows: [],
    fill: "http://fillmurray.com/g/800/400"
  },
  created: function () {
    this.refreshPrices();
  },
  methods: {
    formatCurrencyAsString: function (currency) {
      if (typeof Intl === "undefined" || !Intl.NumberFormat) {
        return currency;
      } else {
        return Intl.NumberFormat().format(currency);                                 // 42.000.000 in many other locales
      }
    },
    refreshPrices: function (event) {
      if (event) {
        // remove focus from button
        event.target.blur();
      }
      var _this = this;
      // this would be a good place to consider debouncing
      _this.nowFetching = true;
      fetch("https://blockchain.info/ticker")
      .then( function(response) {
        _this.lastFetchedTime = new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
        _this.lastFetched = _this.lastFetchedLabel + _this.lastFetchedTime;
        _this.nowFetching = false;
        return response.json()
      })
      .then( function(json) {
        _this.currencyDataJson=json;
        _this.rows = Object.keys(_this.currencyDataJson).map(function(key) {
          return Object.assign({}, _this.currencyDataJson[key], {
            currencyName: key
          })
        });
      });
    }
  }
})
// so... this runs before the data ends up in table...
$(document).ready(function() {
  $('th').addClass("sorting");
  // $('#currencyTable').dataTable({
  //   "paging":   false,
  //   "info":     false,
  //   "searching":   false,
  //   "dom": 'Rlfrtip',
  //   "order": [[ 1, "asc" ]]
  // });
});
